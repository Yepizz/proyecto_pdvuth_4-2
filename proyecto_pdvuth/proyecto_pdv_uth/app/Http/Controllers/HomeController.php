<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;

class HomeController extends Controller
{
    //
    // public function __construct()
    // {
    //     $this->middleware(['auth:sanctum','verified']);
    // }
    public function index()
    {
            $products = Producto::inRandomOrder()->take(8)->get();
            return view('main')->with('products', $products);
    }
}
