<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Models\Producto;

class Carritos extends Component
{
    public $productos;
    public $elementosCarrito = [];
    public $productoId;
    public $numElementosCarrito;
    public $cantActualizada;
    public $cantidades;

    public function mount():void {

         $this->elementosCarrito = Cart::content();

        $this->numElementosCarrito = count($this->elementosCarrito);
        foreach ($this->elementosCarrito as $key => $elementosCarrit)
        {

            $this->productos[$key] = Producto::findOrFail($elementosCarrit->id);
            $this->cantidades[$key] = $this->elementosCarrito[$key]->qty;
        }

        //dd($this->productos, $this->cantidades);

    }

    public function removeFromCart($rowId) {
       Cart::remove($rowId);

    }
    public function render()
    {
        return view('livewire.carritos')->layout('layouts.home');

    }

    public function removeAll(Producto $producto): void {
        Cart::destroy();
        $this->emit('clearCart');
        $this->productos = Cart::content([
            'id' => $producto->id,
            'nombre' => $producto->nombre,
            'precio' =>$producto->precio,
            'descripcion' =>$producto->descripcion,
            'marca' =>$producto->marca,
            'imagen' => $producto->imagen,
        ]);

        $this->productoId = $producto->id;
    }

    public function updateCart($id, $cant){

        // dd($id, $this->cantActualizada[$id], $car);
       Cart::update($id, $cant);
    //    return redirect('/shop');
    //    dd($test);
    }
}