<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;

class Checkout extends Component
{
     public $check = 1;

    public function render()
    {
        return view('livewire.checkouts')->layout('layouts.home');
    }

    public function validarCampos(){

        dd('llego algo');
        //Validar todos los campos del formulario de Checkouts...
        $this->validate([
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'neighborhood' => 'required',
            'country' => 'required',
            'postal_code' => 'required',
            'city' => 'required',
        ]);

    }
}
