<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Cliente;

class Clientes extends Component
{
    public $clientes, $clienteId,$nombre, $apellidoPaterno,$apellidoMaterno,
    $fechaNacimiento, $celular, $correo, $metodoPago, $correoPago,
    $numeroTarjeta, $vigencia, $numSecreto, $bancoEmisor,$pais, $password,
    $password_confirm,$calle, $numeroCasa, $localidad, $municipio, $codigoPostal;

    public $isOpen = 0;

    public function render()
    {
        $this->clientes  = Cliente::all();
        return view('livewire.clientes');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function openModal()
    {
        $this->isOpen = true;
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function closeModal()
    {
        $this->isOpen = false;

    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private function resetInputFields(){

        $this->clienteId = '';
        $this->nombre = '';
        $this->apellidoPaterno = '';
        $this->apellidoMaterno = '';
        $this->fechaNacimiento = '';
        $this->celular = '';
        $this->correo = '';
        $this->metodoPago = '';
        $this->correoPago = '';
        $this->numeroTarjeta = '';
        $this->vigencia = '';
        $this->numSecreto = '';
        $this->bancoEmisor = '';
        $this->pais = '';
        $this->password = '';
        $this->password_confirm = '';
        $this->calle = '';
        $this->numeroCasa = '';
        $this->localidad = '';
        $this->municipio = '';
        $this->codigoPostal = '';

    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {
        $this->validate([
            'nombre' => 'required',
            'apellidoPaterno' => 'required',
            'fechaNacimiento' => 'required',
            'celular' => 'required',
            'correo' => 'required',
            'metodoPago' => 'required',
            'correoPago' => 'required',
            'numeroTarjeta' => 'required',
            'vigencia' => 'required',
            'numSecreto' => 'required',
            'bancoEmisor' => 'required',
            'pais' => 'required',
            'password' => 'required',
            'password_confirm' => 'required',
            'calle' => 'required',
            'numeroCasa' => 'required',
            'localidad' => 'required',
            'municipio' => 'required',
            'codigoPostal' => 'required',
        ]);

        Cliente::updateOrCreate(['id' => $this->clienteId], [

            'nombre' => $this->nombre,
            'apellidoPaterno' => $this->apellidoPaterno,
            'apellidoMaterno' => $this->apellidoPaterno,
            'fechaNacimiento' => $this->fechaNacimiento,
            'celular' => $this->celular,
            'correo' => $this->correo,
            'metodoPago' => $this->metodoPago,
            'correoPago' => $this->correoPago,
            'numeroTarjeta' => $this->numeroTarjeta,
            'vigencia' => $this->vigencia,
            'numSecreto' => $this->numSecreto,
            'bancoEmisor' => $this->bancoEmisor,
            'pais' => $this->pais,
            'password' => $this->password,
            'password_confirm' => $this->password_confirm,
            'calle' => $this->calle,
            'numeroCasa' => $this->numeroCasa,
            'localidad' => $this->localidad,
            'municipio' => $this->municipio,
            'codigoPostal' => $this->codigoPostal,

        ]);
        session()->flash('message',
        $this->clienteId ? 'Cliente Actualizado Exitosamente.' : 'Cliente Creado Exitosamente.');
        $this->closeModal();
        $this->resetInputFields();
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function edit($id)
    {
        $cli = Cliente::findOrFail($id);
        $this->clienteId = $id;
        $this->nombre = $cli->nombre;
        $this->apellidoPaterno = $cli->apellidoPaterno;
        $this->apellidoMaterno = $cli->apellidoMaterno;
        $this->fechaNacimiento = $cli->fechaNacimiento;
        $this->celular = $cli->celular;
        $this->correo = $cli->correo;
        $this->metodoPago = $cli->metodoPago;
        $this->correoPago = $cli->correoPago;
        $this->numeroTarjeta = $cli->numeroTarjeta;
        $this->vigencia = $cli->vigencia;
        $this->numSecreto = $cli->numSecreto;
        $this->bancoEmisor = $cli->bancoEmisor;
        $this->pais = $cli->pais;
        $this->password = $cli->password;
        $this->password_confirm = $cli->password_confirm;
        $this->calle = $cli->calle;
        $this->numeroCasa = $cli->numeroCasa;
        $this->localidad = $cli->localidad;
        $this->municipio = $cli->municipio;
        $this->codigoPostal = $cli->codigoPostal;
        $this->openModal();

    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function delete($id)
    {
        Cliente::find($id)->delete();
        session()->flash('message', 'Cliente Eliminado Satisfactoriamente.');
    }

}
