<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Homes extends Component
{
    // public function __construct()
    // {
    //     $this->middleware(['auth:sanctum','verified']);
    // }
    public function render()
    {
   
        return view('livewire.homes')->layout('layouts.home') ;
    }
}