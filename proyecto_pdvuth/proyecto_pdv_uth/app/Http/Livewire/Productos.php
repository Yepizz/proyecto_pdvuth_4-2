<?php

namespace App\Http\Livewire;


use Livewire\Component;
use App\Models\Producto;
use Livewire\WithFileUploads;


class Productos extends Component
{
    use WithFileUploads;

    public $productos, $productoId,
        $nombre, $descripcion, $precio, $marca,
        $categoria, $codigoBarras, $imagen, $unidadMedida;
    public $isOpen = 0;

    public function render()
    {
        $this->productos = Producto::all();
        return view('livewire.productos');
    }

    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {

        $this->isOpen = false;
    }

    private function resetInputFields()
    {
        $this->productoId = '';
        $this->nombre = '';
        $this->descripcion = '';
        $this->precio = '';
        $this->marca = '';
        $this->categoria = '';
        $this->codigoBarras = '';
        $this->imagen = '';
        $this->unidadMedida = '';
    }

    public function store()
    {

        $validatedData = $this->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
            'precio' => 'required',
            'marca' => 'required',
            'categoria' => 'required',
            'codigoBarras' => 'required|max:13',
            'imagen' => 'required',
            // 'imagen' => 'required | image | mimes:jpeg,jpg,png,bmp,gif,svg | max:2048',
            'unidadMedida' => 'required',
        ]);
        $validatedData['imagen'] = $this->imagen->store('public/imagenes');
        $validatedData['imagen'] = str_replace('public/imagenes/', '', $validatedData['imagen']);
        $prod = Producto::create($validatedData);
        session()->flash(
            'message',
            $this->productoId ? 'Producto Modificado Satisfactoriamente.' : 'Producto Creado Satisfactoriamente.'
        );

        $this->closeModal();

        $this->resetInputFields();
    }

    public function edit($id)
    {

        $producto = Producto::findOrFail($id);
        $this->productoId = $id;
        $this->nombre = $producto->nombre;
        $this->descripcion = $producto->descripcion;
        $this->precio = $producto->precio;
        $this->marca = $producto->marca;
        $this->categoria = $producto->categoria;
        $this->codigoBarras = $producto->codigoBarras;
        $this->imagen = $producto->imagen;
        $this->unidadMedida = $producto->unidadMedida;

        $this->openModal();
    }

    public function delete($id)
    {

        Producto::find($id)->delete();

        session()->flash('message', 'Producto Borrado Satisfactoriamente.');
    }


}
