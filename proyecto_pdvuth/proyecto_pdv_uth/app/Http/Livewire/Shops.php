<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Producto;
use Illuminate\View\View;
use Livewire\WithPagination;
use Gloudemans\Shoppingcart\Facades\Cart;

class Shops extends Component
{
    use WithPagination;
    /**Vars for search */
    public $search;
    protected $updatesQueryString = ['search'];
    #Vars for cart
    public $cartTotal = 0;
    public $elementosCarrito;
    public $numArticulos;
    public $carrrito;
    protected $listeners = [
        'productAdded' => 'updateCartTotal',
        'productRemoved' => 'updateCartTotal',
        'clearCart' => 'updateCartTotal'
    ];

     public function render(): View
    {
        return view('livewire.shops',['products' => $this->search === null ?
            Producto::paginate(12) : Producto::where('nombre', 'like', '%'.$this->search.'%')->paginate(12)
        ])->layout('layouts.home');
    }

     public function mount(): void
    {
        $this->elementosCarrito = Cart::content();
        $this->numArticulos = count($this->elementosCarrito);
    }
    public function search():void {
        $this->search = request()->query('search',$this->search);
    }

    public function addToCart(int $productId):void {
        Cart::add(Producto::where('id', $productId)->first());
        $this->emit('productAdded');
    }


     public function updateCartTotal($producto)
    {
        return null;
        foreach($this->elementosCarrito as $key=> $elementosCarrit){
            dd($elementosCarrit->rowIndex);
        }
    }

    public function clearCart(){
        Cart::destroy();
    }
}
