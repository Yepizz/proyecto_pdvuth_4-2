<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    protected $table = "clientes";

    protected $fillable = [
        'nombre',
        'apellidoPaterno',
        'apellidoMaterno',
        'fechaNacimiento',
        'celular',
        'correo',
        'metodoPago',
        'correoPago',
        'numeroTarjeta',
        'vigencia',
        'numSecreto',
        'bancoEmisor',
        'pais',
        'password',
        'password_confirm',
        'calle',
        'numeroCasa',
        'localidad',
        'municipio',
        'codigoPostal',
    ];
}
