<?php

namespace App\Models;

use App\Models\User;
use App\Models\VentasDetalle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Venta extends Model
{
    use HasFactory;

    // Método para la llave foranea de los items del carrito con el usuario...
    public function itemsCart(){
        return $this->belongsTo(VentasDetalle::class,  'producto_id', 'venta_id');
    }

    //Método user

    public function userId(){

        return $this->belongsTo(User::class, 'id');

    }
}
