<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VentasDetalle extends Model
{
    use HasFactory;

    protected $table = "ventas_detalles";

    protected $fillable = [

        'producto_id', 'cantidad','venta_id','precio'
    ];
}
