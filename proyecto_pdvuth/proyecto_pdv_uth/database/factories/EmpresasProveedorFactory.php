<?php

namespace Database\Factories;

use App\Models\EmpresasProveedor;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmpresasProveedorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmpresasProveedor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
