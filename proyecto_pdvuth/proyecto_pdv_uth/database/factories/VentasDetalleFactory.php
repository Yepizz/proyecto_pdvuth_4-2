<?php

namespace Database\Factories;

use App\Models\VentasDetalle;
use Illuminate\Database\Eloquent\Factories\Factory;

class VentasDetalleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = VentasDetalle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
