<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('last_name',20);
            $table->string('email')->nullable();
            $table->string('phone',10);
            $table->string('neighborhood',20);
            $table->string('country',20);
            $table->string('postal_code',5);
            $table->string('city',15);
            $table->string('card_number',18)->nullable();
            $table->string('month_vigency')->nullable();
            $table->string('year_vigency')->nullable();
            $table->string('secret_number')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->string('user_type')->default('CUSTOMER')->comment('ADMIN and CUSTOMER only!');
            $table->timestamps();
            // $table->string('avatar')->nullable();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
