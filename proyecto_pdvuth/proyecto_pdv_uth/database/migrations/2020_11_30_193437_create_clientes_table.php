<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellidoPaterno');
            $table->string('apellidoMaterno')->nullable();
            $table->date('fechaNacimiento');
            $table->string('celular');
            $table->string('correo');
            $table->enum('metodoPago',['PAYPAL','EFECTIVO','TARJETA_CREDITO','MERCADO_PAGO'])->nullable();
            $table->string('correoPago')->nullable();
            $table->string('numeroTarjeta',12)->nullable();
            $table->string('vigencia')->nullable();
            $table->string('numSecreto')->nullable();
            $table->enum('bancoEmisor',['BANCOMER','CITY_BANAMEX','BANCO_AZTECA','SANTANDER'])->nullable();
            $table->string('pais');
            $table->string('password');
            $table->string('password_confirm');
            $table->string('calle');
            $table->string('numeroCasa');
            $table->string('localidad');
            $table->string('municipio');
            $table->string('codigoPostal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
