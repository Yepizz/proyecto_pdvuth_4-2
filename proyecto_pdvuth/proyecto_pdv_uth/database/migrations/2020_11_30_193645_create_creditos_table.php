<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('creditos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->double('saldo');
            $table->date('fecha_apertura');
            $table->enum('estado',['APERTURA','OPERANDO','CONGELADO','SALDADO']);
            $table->date('fecha_saldado');
            $table->unsignedBigInteger('cliente_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creditos');
    }
}
