<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_empresa',55);
            $table->integer('num_identificador')->length(10)->unsigned();;
            $table->integer('codigo_postal')->length(5)->unsigned();;
            $table->string('telefono',10);
            $table->string('correo',30);
            $table->string('calle',15);
            $table->string('colonia',15);
            $table->string('ciudad',15);
            $table->string('estado',15);
            $table->string('pais',15);
            $table->string('municipio',15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
