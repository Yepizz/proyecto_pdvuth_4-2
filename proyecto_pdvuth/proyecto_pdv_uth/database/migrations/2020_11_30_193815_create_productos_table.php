<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->text('descripcion');
            $table->double('precio');
            $table->string('marca',75);
            $table->enum('categoria', ['SOPAS', 'REFRESCOS','HIGIENE','FRUTAS_Y_VERDURAS','SUAVISANTE_ROPA','AGUA_EMBOTELLADA','CERVEZAS','SALSAS','CREMAS_Y_DESODORANTES','DERIVADOS_LACTEOS','UNTABLES','ACEITES','COMIDA_PARA_BEBE','CAFE_Y_TE','SABRITAS','DULCES']);
            $table->string('codigoBarras',13);
            $table->string('imagen');
            $table->enum('unidadMedida',['UNIDAD','LITRO','KILO','CAJA','PAQUETE']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}