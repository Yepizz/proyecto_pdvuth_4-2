<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProveedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('proveedores', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido_paterno',20);
            $table->string('apellido_materno',20)->nullable();
            $table->string('colonia',20);
            $table->string('localidad',20);
            $table->string('calle',20);
            $table->integer('codigo_postal')->length(5)->unsigned();;
            $table->string('celular',10);
            $table->string('correo',20);
            $table->boolean('activo');
            $table->integer('codigo_identificador')->length(13)->unsigned();;
            $table->string('empresas_maneja',95);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedors');
    }
}
