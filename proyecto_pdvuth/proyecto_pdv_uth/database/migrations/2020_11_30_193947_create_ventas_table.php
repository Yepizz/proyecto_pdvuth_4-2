<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cliente_id');
            $table->string('folio',13)->default(Carbon::now()->timestamp);
            $table->date('fecha_hora');
            $table->double('total_venta')->unsigned();
            $table->enum('metodo_pago',['EFECTIVO','TARJETA_CREDITO','PAYPAL','MERCADO_PAGO']);
            $table->integer('num_productos_vendidos')->length(10)->unsigned();
            $table->string('numero_tarjeta',16)->nullable();
            $table->string('mes_vigencia',2)->nullable();
            $table->string('anio_vigencia',2)->nullable();
            $table->string('cuenta_pasarela',50)->nullable();
            $table->string('num_secreto',50)->nullable();
            $table->boolean('cancelado');
            $table->timestamps();
            $table->foreign('cliente_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::table('ventas', function (Blueprint $table) {
            //
            $table->dropForeign('cliente_id');
        });

    }
}
