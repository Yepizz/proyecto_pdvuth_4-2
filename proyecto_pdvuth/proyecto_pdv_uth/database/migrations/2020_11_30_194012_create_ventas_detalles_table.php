<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('ventas_detalles', function (Blueprint $table) {
            $table->unsignedBigInteger('producto_id');
            $table->integer('cantidad')->length(10)->unsigned();
            $table->unsignedBigInteger('venta_id');
            $table->double('precio');

            $table->foreign('venta_id')->references('id')->on('ventas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ventas_detalles', function (Blueprint $table) {
            //
            $table->dropForeign('venta_id');
        });
    }
}
