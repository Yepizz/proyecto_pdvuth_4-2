<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $productos = [
            //fields here

            "1"=>[
                'id'=>1,
                'nombre'=>'Sabritas Original',
                'descripcion'=>'Papas amarillas 64 g',
                'precio'=> 29.99,
                'marca'=>'Doritos',
                'categoria'=>'SABRITAS',
                'codigoBarras'=>'7501232345123',
                'imagen'=> 'https://http2.mlstatic.com/D_NQ_NP_640646-MLM44024270446_112020-O.webp',
                'unidadMedida'=>'UNIDAD',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),

            ]

        ];

        foreach($productos as $key=>$product){
            DB::table('productos')->insert($product);
        }


    }
}