<?php

namespace Database\Seeders;


use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'name' => 'Israel Guadalupe',
                'email' => 'yepiz@gmail.com',
                'last_name' => 'Yepiz',
                'phone' => '6622001809',
                'neighborhood' => 'Apache',
                'country' => 'México',
                'postal_code' => '83000',
                'city' => 'Hermosillo',
                'email_verified_at' => now(),
                'password' => Hash::make('123123123'), // password
                'user_type' => 'ADMIN', // password
                'remember_token' => Str::random(10),
            ],


        );
    }
}
