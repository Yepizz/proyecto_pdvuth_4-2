function showForm(method) {

        if (method === "CASH") {
            document.getElementById("card-form").style.display = "none";
            document.getElementById("paypal").style.display = "none";
            document.getElementById("paid-market").style.display = "none";
        } else if (method === "CREDIT_CARD") {
            document.getElementById("card-form").style.display = "block";
            document.getElementById("paypal").style.display = "none";
            document.getElementById("paid-market").style.display = "none";
        } else if (method === "PAYPAL") {
            document.getElementById("card-form").style.display = "none";
            document.getElementById("paypal").style.display = "block";
            document.getElementById("paid-market").style.display = "none";
        } else if (method === "PAID_MARKET") {
            document.getElementById("card-form").style.display = "none";
            document.getElementById("paypal").style.display = "none";
            document.getElementById("paid-market").style.display = "block";
        } else {
            document.getElementById("card-form").style.display = "none";
            document.getElementById("paypal").style.display = "none";
            document.getElementById("paid-market").style.display = "none";
        }
    }