{{-- Inicio de home del carrito de compras de PDV UTH! --}}
<div class="col-12">
    <div class="table-responsive">
        @if (count($productos) > 0)
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Image</th>
                        <th scope="col">Description</th>
                        <th scope="col" class="text-left">Quantity</th>
                        <th scope="col" class="text-right">Price</th>
                        <th scope="col" class="text-right">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- {{ dd($productos) }} --}}
                    @foreach ($productos as $index => $product)

                        <tr>
                            <td><img src="{{ asset('storage/imagenes/' . $product['imagen']) }}" height="80px"
                                    width="80px" /></td>
                            <td>{{ $product['nombre'] }}</td>

                            <td><input defer class="form-control" type="number" id="{{ $index }}"
                                    value="{{ $this->cantidades[$index] }}"
                                    wire:change="updateCart('{{ $index }}',document.getElementById('{{ $index }}').value)" />
                            </td>
                            <td class="text-right">{{ $product['precio'] }}</td>
                            <td>
                                <a wire:click="removeFromCart({{ $product->rowId }})"
                                    class="text-green-600 font-bold py-1 px-3 rounded text-xs bg-green hover:bg-green-dark cursor-pointer">X</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tr>
                    <td>Sub-Total</td>
                    <td class="text-right">{{ Cart::subTotal() }}</td>
                </tr>
                <tr>
                    <td>Tax</td>
                    <td class="text-right">{{ Cart::tax() }}</td>
                </tr>
                <tr>
                    <td><strong>Total</strong></td>
                    <td class="text-right"><strong>{{ Cart::total() }}</strong></td>
                </tr>
            </table>
    </div>
    <div class="col mb-2">
        <div class="row">
            <div class="col-sm-12  col-md-6">
                <button wire:click="removeAll()" class="btn btn-danger">Clear Cart
                </button>
            </div>
            <div class="col-sm-12  col-md-6">
                <a style="text-decoration:none" href="/shop"><button class="btn btn-block btn-light">Continue
                        Shopping</button></a>
            </div>
            <div class="col-sm-12 col-md-6 text-right">
                <a style="text-decoration:none" href="/checkout"><button
                        class="btn btn-block btn-light">Checkout</button></a>
            </div>
        </div>
    @else
        <div class="text-center w-full border-collapse p-6">
            <span class="text-lg">¡Your cart is empty!</span>
        </div>
        @endif
    </div>
</div>
