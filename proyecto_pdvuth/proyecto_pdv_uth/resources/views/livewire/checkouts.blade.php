{{-- <script>
    function showForm(method) {

        if (method === "CASH") {
            document.getElementById("card-form").style.display = "none";
            document.getElementById("paypal").style.display = "none";
            document.getElementById("paid-market").style.display = "none";
        } else if (method === "CREDIT_CARD") {
            document.getElementById("card-form").style.display = "block";
            document.getElementById("paypal").style.display = "none";
            document.getElementById("paid-market").style.display = "none";
        } else if (method === "PAYPAL") {
            document.getElementById("card-form").style.display = "none";
            document.getElementById("paypal").style.display = "block";
            document.getElementById("paid-market").style.display = "none";
        } else if (method === "PAID_MARKET") {
            document.getElementById("card-form").style.display = "none";
            document.getElementById("paypal").style.display = "none";
            document.getElementById("paid-market").style.display = "block";
        } else {
            document.getElementById("card-form").style.display = "none";
            document.getElementById("paypal").style.display = "none";
            document.getElementById("paid-market").style.display = "none";
        }
    }

</script> --}}
<main id="main" class="main-site">

    <div class="container">

        <div class="wrap-breadcrumb">
            <ul>
                <li class="item-link"><a href="/" class="link">home</a></li>
                <li class="item-link"><span>login</span></li>
            </ul>
        </div>
        <div class=" main-content-area">
            <div class="wrap-address-billing">
                <h3 class="box-title">Shipping Information</h3>
                <form action="/thankyou" method="POST">
                    @csrf
                    <p class="row-in-form">
                        <label for="name">Name<span>*</span></label>
                        <input id="name" wire:change="store1" type="text" name="name"
                            value="{{ Auth::user() ? Auth::user()->name : '' }}" placeholder="Your name...">
                    </p>
                    <p class="row-in-form">
                        <label for="last_name">Last Name<span>*</span></label>
                        <input id="last_name" type="text" name="last_name"
                            value="{{ Auth::user() ? Auth::user()->last_name : '' }}">
                    </p>
                    <p class="row-in-form">
                        <label for="email">Email Addreess:<span>*</span></label>
                        <input id="email" type="email" name="email"
                            value="{{ Auth::user() ? Auth::user()->email : '' }}">
                    </p>
                    <p class="row-in-form">
                        <label for="phone">Phone number<span>*</span></label>
                        <input id="phone" type="number" name="phone"
                            value="{{ Auth::user() ? Auth::user()->phone : '' }}">
                    </p>
                    <p class="row-in-form">
                        <label for="neighborhood">Neighborhood<span>*</span></label>
                        <input id="neighborhood" type="text" name="neighborhood"
                            value="{{ Auth::user() ? Auth::user()->neighborhood : '' }}" </p>
                    <p class="row-in-form">
                        <label for="country">Country<span>*</span></label>
                        <input id="country" type="text" name="country"
                            value="{{ Auth::user() ? Auth::user()->country : '' }}" placeholder="México">
                    </p>
                    <p class="row-in-form">
                        <label for="postal_code">Postal Code<span>*</span></label>
                        <input id="postal_code" type="number" name="postal_code"
                            value="{{ Auth::user() ? Auth::user()->postal_code : '' }}" placeholder="Your postal code">
                    </p>
                    <p class="row-in-form">
                        <label for="city">City<span>*</span></label>
                        <input id="city" type="text" name="city" value="{{ Auth::user() ? Auth::user()->city : '' }}"
                            placeholder="City name">
                    </p>

                    {{-- DIV FOR CREDIT CARD --}}
                    <div id="card-form">
                        <div class="row-in-form">
                            <label for="card_number">Card Number<span>*</span></label>
                            <input id="card_number" type="text" name="card_number" placeholder="1234 1234 1234 1234" />
                        </div>
                        <div class="row-in-form">
                            <label for="month_vigency">Month Vigency<span>*</span></label>
                            <input id="month_vigency" type="text" name="month_vigency" />
                        </div>
                        <div class="row-in-form">
                            <label for="year_vigency">Year Vigency<span>*</span></label>
                            <input id="year_vigency" type="text" name="year_vigency" placeholder="2020" />
                        </div>
                        <div class="row-in-form">
                            <label for="secret_number">Secret Number<span>*</span></label>
                            <input id="secret_number" type="text" name="secret_number" placeholder="1234" />
                        </div>
                    </div>
                    {{-- END DIV CREDIT CARD --}}

                    {{-- DIV FOR METHOD PAID MARKET --}}
                    <div id="paid-market">
                        <div class="row-in-form">
                            <label for="email_paid">Email For Paid Market</label>
                            <input id="email_paid" type="email" name="email_paid" placeholder="Your Email" />
                        </div>
                        <div class="row-in-form">
                            <label for="password_paid">Password For Paid Market<span>*</span></label>
                            <input id="password_paid" type="text" name="password_paid" placeholder="Your Password" />
                        </div>
                    </div>
                    {{-- END DIV METHOD PAID MARKET --}}

                    {{-- DIV FOR PAYPAL --}}
                    <div id="paypal">
                        <div class="row-in-form">
                            <label for="email_paypal">Email For Paypal</label>
                            <input id="email_paypal" type="email" name="email_paypal" placeholder="Your Email..." />
                        </div>
                        <div class="row-in-form">
                            <label for="password_paypal">Password For Paypal<span>*</span></label>
                            <input id="password_paypal" type="text" name="password_paypal"
                                placeholder="Your Password..." />
                        </div>
                    </div>
                    {{-- END DIV PAYPAL --}}
                </form>
                <label for="">Choose Method Paid<span>*</span></label>
                <select class="row-in-form" onchange="showForm(this.value)" name="method_payment" id="method_payment">
                    <option value="">Choose...</option>
                    <option value="CASH">Cash</option>
                    <option value="CREDIT_CARD">Credit Card</option>
                    <option value="PAID_MARKET">Paid Market</option>
                    <option value="PAYPAL">PAYPAL</option>
                </select>
            </div>
            <div class="summary summary-checkout">
                <div id="paypal-button">

                </div>
                {{-- {{ dd($check) }} --}}
                <div class="summary-item payment-method">
                    <p class="summary-info grand-total"><span>Grand Total</span> <span
                            class="grand-total-price">$100.00</span></p>
                    <button wire:click="validarCampos" class="btn btn-medium">Place order
                        now</button>

                </div>
                <div class="summary-item shipping-method">
                    <h4 class="title-box f-title">Shipping method</h4>
                    <p class="summary-info"><span class="title">Flat Rate</span></p>
                    <p class="summary-info"><span class="title">Fixed $50.00</span></p>
                    <h4 class="title-box">Discount Codes</h4>
                    <p class="row-in-form">
                        <label for="coupon-code">Enter Your Coupon code:</label>
                        <input id="coupon-code" type="text" name="coupon-code" value="" placeholder="">
                    </p>
                    <button wire:click="store1()" class="btn btn-small">Apply</button>
                </div>
            </div>
        </div>
    </div>
    {{-- <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    --}}
    {{-- <script>
        paypal.Button.render({
            env: 'sandbox', // Or 'production'
            style: {
                size: 'large',
                color: 'gold',
                shape: 'pill',
            },
            // Set up the payment:
            // 1. Add a payment callback
            payment: function(data, actions) {
                // 2. Make a request to your server
                return actions.request.post('/api/create-payment/')
                    .then(function(res) {
                        // 3. Return res.id from the response
                        return res.id;
                    });
            },
            // Execute the payment:
            // 1. Add an onAuthorize callback
            onAuthorize: function(data, actions) {
                // 2. Make a request to your server
                return actions.request.post('/api/execute-payment/', {
                        paymentID: data.paymentID,
                        payerID: data.payerID
                    })
                    .then(function(res) {
                        // 3. Show the buyer a confirmation message.
                    });
            }
        }, '#paypal-button');

    </script> --}}
    {{-- <script>
        paypal.Button.render({
            // Configure environment
            env: 'sandbox',
            client: {
                sandbox: 'AXP_-r5xcvvLsn55a0GXjZ-4YifbpQKNNlWw7_8E3utUPHSpGe8tb9zRoPah706kvuibLk0l_YT9RiLP',
                production: 'demo_production_client_id'
            },
            // Customize button (optional)
            locale: 'en_US',
            style: {
                size: 'large',
                color: 'gold',
                shape: 'pill',
            },

            // Enable Pay Now checkout flow (optional)
            commit: true,

            // Set up a payment
            payment: function(data, actions) {
                return actions.payment.create({
                    transactions: [{
                        amount: {
                            total: '299.00',
                            currency: 'MXN'
                        }
                    }]
                });
            },
            // Execute the payment
            onAuthorize: function(data, actions) {
                return actions.payment.execute().then(function() {
                    // Show a confirmation message to the buyer
                    window.alert('Thank you for your purchase!');
                });
            }
        }, '#paypal-button');

    </script> --}}
</main>
