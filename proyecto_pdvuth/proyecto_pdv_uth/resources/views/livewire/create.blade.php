<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
  <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
    <div class="fixed inset-0 transition-opacity">
      <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
    </div>
    <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>​
    <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
      <form method="post" enctype="multipart/form-data">
      <div class="bg-white px-6 pt-5 pb-4 sm:p-6 sm:pb-4">
        <div class="">
              <div class="mb-4">
                  <label for="exampleFormControlInput1" class="block text-gray-700 text-sm font-bold mb-2">Nombre:</label>
                  <input type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="exampleFormControlInput1" placeholder="Escriba el nombre" wire:model="nombre" autofocus>
                  @error('nombre') <span class="text-red-500">{{ $message }}</span>@enderror
              </div>
              <div class="mb-4">
                  <label for="exampleFormControlInput2" class="block text-gray-700 text-sm font-bold mb-2">Descripción:</label>
                  <textarea class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="exampleFormControlInput2" wire:model="descripcion" placeholder="Escriba Descripción"></textarea>
                  @error('descripcion') <span class="text-red-500">{{ $message }}</span>@enderror
              </div>
              <div class="mb-4">
                  <label for="exampleFormControlInput1" class="block text-gray-700 text-sm font-bold mb-2">Precio:</label>
                  <input type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="exampleFormControlInput1" placeholder="Escriba el precio" wire:model="precio">
                  @error('precio') <span class="text-red-500">{{ $message }}</span>@enderror
              </div>
              <div class="mb-4">
                  <label for="exampleFormControlInput1" class="block text-gray-700 text-sm font-bold mb-2">Marca:</label>
                  <input type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="exampleFormControlInput1" placeholder="Escriba la marca" wire:model="marca">
                  @error('marca') <span class="text-red-500">{{ $message }}</span>@enderror
              </div>
              <div class="mb-4">
                  <label for="exampleFormControlInput1" class="block text-gray-700 text-sm font-bold mb-2">Categoría:</label>
                  <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  wire:model="categoria" id="">
                      <option class="block" value="">Seleccione uno...</option>
                      <option class="block" value="SOPAS">Sopas</option>
                      <option class="block" value="REFRESCOS">Refrescos</option>
                      <option class="block" value="HIGIENE">Higiene</option>
                      <option class="block" value="FRUTAS_Y_VERDURAS">Frutas y verduras</option>
                      <option class="block" value="SUAVISANTE_ROPA">Suavisante de ropa</option>
                      <option class="block" value="AGUA_EMBOTELLADA">Agua embotellada</option>
                      <option class="block" value="CERVEZAS">Cervezas</option>
                      <option class="block" value="SALSAS">Salsas</option>
                      <option class="block" value="CREMAS_Y_DESODORANTES">Cremas y desodorantes</option>
                      <option class="block" value="DERIVADOS_LACTEOS">Derivados Lácteos</option>
                      <option class="block" value="UNTABLES">Untables</option>
                      <option class="block" value="ACEITES">Aceites</option>
                      <option class="block" value="COMIDA_PARA_BEBE">Comida para bebé</option>
                      <option class="block" value="CAFE_Y_TE">Cafe y té</option>
                      <option class="block" value="SABRITAS">Sabritas</option>
                      <option class="block" value="DULCES">Dulces</option>
                  </select>
                  @error('categoria') <span class="text-red-500">{{ $message }}</span>@enderror
              </div>
              <div class="mb-4">
                  <label for="exampleFormControlInput1" class="block text-gray-700 text-sm font-bold mb-2">Código de barras:</label>
                  <input type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="exampleFormControlInput1" name="codigo_barras" placeholder="Escriba el código de barras" wire:model="codigoBarras">
                  @error('codigoBarras') <span class="text-red-500">{{ $message }}</span>@enderror
              </div>
              <div class="mb-4">
                  <label for="exampleFormControlInput1" class="block text-gray-700 text-sm font-bold mb-2">Imagen:</label>
                <p class="block">
                            Sube una imagen
                            <input  class="shadow appearance-none border rounded w-full py-2 px-3 text-blue-900" type="file" wire:model="imagen" name="imagen" id="imagen">
                        </p>
                  @error('imagen') <span class="text-red-500">{{ $message }}</span>@enderror
              </div>
              <div class="mb-4">
                  <label for="exampleFormControlInput1" class="block text-gray-700 text-sm font-bold mb-2">Unidad Medida:</label>
                  <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"  wire:model="unidadMedida" id="">
                      <option class="block" value="">Seleccione uno...</option>
                      <option class="block" value="UNIDAD">Unidad</option>
                      <option class="block" value="LITRO">Litro</option>
                      <option class="block" value="KILO">Kilo</option>
                      <option class="block" value="CAJA">Caja</option>
                      <option class="block" value="PAQUETE">Paquete</option>
                  </select>
                  @error('unidadMedida') <span class="text-red-500">{{ $message }}</span>@enderror
              </div>
        </div>
      </div>
      <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
          <button wire:click.prevent="store()" type="button" class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-green-500 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
            Guardar
          </button>
        </span>
        <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
          <button wire:click="closeModal()" type="button" class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
            Cancelar
          </button>
        </span>
        </form>
      </div>
    </div>
  </div>
</div>
