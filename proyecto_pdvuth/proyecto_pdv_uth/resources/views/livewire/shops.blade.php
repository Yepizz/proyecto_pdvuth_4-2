<main id="main" class="main-site left-sidebar">


    </div>
    </div>

    <div class="container">
        <div class="wrap-breadcrumb">
            <ul>
                <li class="item-link"><a href="/" class="link">home</a></li>
                <li class="item-link"><span>Products</span></li>
                {{-- <li class="item-link"><a href="/carritoCompras">Number of articles:
                        &nbsp; &nbsp; &nbsp;
                        Cart({{ $numArticulos }})</a>
                </li> --}}
                {{-- <button wire:click="clearCart()" class="btn btn-danger">
                    Clear Cart!
                </button> --}}
            </ul>
        </div>
        <div class="row">
            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 main-content-area">
                <div class="box-wrapper">
                    <h2>¡Search products by name!</h2>
                    <div class=" bg-white rounded flex items-center w-full p-3 shadow-sm border border-gray-200">
                        <input type="search" wire:model="search" name="" id="" placeholder="Search By Name"
                            class="form-control">
                    </div>
                    <div class="wrap-shop-control">
                        <h1 class="shop-title" style="text-align:center">Products Available</h1>
                    </div>
                    <!--end wrap shop control-->
                    <div class="row">
                        {{-- @php
                        dd($products);
                        @endphp --}}
                        @foreach ($products as $producto)

                            <ul class="product-list grid-products equal-container">
                                <li class="col-lg-4 col-md-6 col-sm-6 col-xs-6 ">
                                    <div class="product product-style-3 equal-elem ">
                                        <div class="product-thumnail">
                                            <a href="/detalles/{{ $producto->id }}" title="{{ $producto->name }}">
                                                <figure><img src="{{ asset('storage/imagenes/' . $producto->imagen) }}"
                                                        height="220px" width="220px"></figure>
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <a href="/detalles"
                                                class="product-name"><span>{{ $producto->nombre }}</span></a>
                                            <div class="wrap-price"><span
                                                    class="product-price">{{ $producto->precio }}</span></div>
                                            <a wire:click="addToCart({{ $producto->id }})"
                                                href="agregar/{{ $producto->id }}">Add to cart!
                                            </a>&nbsp;&nbsp; &nbsp;
                                            <a href="/detalles/{{ $producto->id }}">Show Product
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
</main>
