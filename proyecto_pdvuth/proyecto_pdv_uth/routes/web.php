<?php

use App\Models\Producto;
use App\Http\Livewire\Homes;
use App\Http\Livewire\Shops;
use App\Http\Livewire\Carritos;
use App\Http\Livewire\Checkout;
use App\Http\Livewire\Clientes;
use App\Http\Livewire\Checkouts;
use App\Http\Livewire\Contactos;
use App\Http\Livewire\Productos;
use Illuminate\Support\Facades\Route;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Http\Controllers\ProductoController;
use App\Http\Livewire\User\UserDashboardComponent;
use App\Http\Controllers\Auth\SocialAuthController;
use App\Http\Controllers\CheckoutController;
use App\Http\Livewire\Admin\AdminDashboardComponent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
//Ruta para el cliente
Route::middleware(['auth:sanctum','verified'])->group(function(){
    Route::get('/', Homes::class);
});
//Ruta para ADMIN
Route::middleware(['auth:sanctum','verified','authadmin'])->group(function(){
    Route::get('/admin/dashboard', AdminDashboardComponent::class)->name('admin.dashboard');
});

Route::get('productos', Productos::class);

Route::get('clientes', Clientes::class);

Route::get('/', Homes::class);

Route::get('/shop',Shops::class)->name('livewire.shop');

// Route::get('/carrito' , Carritos::class);

// Route::get('agregar/{id}', function ($id) {

//     $carrito = Producto::findOrFail($id);

//     $cart = ['id' => $carrito->id,
//     'name' => $carrito->nombre,
//     'description' => $carrito->descripcion,
//     'price' => $carrito->precio,
//     'imagen'=>$carrito->imagen,
//     'brand' => $carrito->marca,
//     'weight' => 0,
//     'qty' => 1];

//     $result= Cart::add($cart);
//     return redirect()->route('livewire.shop');

// });
Route::middleware(['auth:sanctum', 'verified'])->get('agregar/{id}', function ($id)
{
    $carrito = Producto::findOrFail($id);
    $cart = ['id' => $carrito->id,
        'name' => $carrito->nombre,
        'description' => $carrito->descripcion,
        'price' => $carrito->precio,
        'imagen' => $carrito->imagen,
        'brand' => $carrito->marca,
        'weight' => 0,
        'qty' => 1 ];
    $result = Cart::add($cart);
    return redirect()->route('livewire.shop');
})->name('addToCart');

Route::get('/paypal/pay', 'PaymentController@payWithPayPal');
Route::get('/paypal/status', 'PaymentController@payPalStatus');

Route::get('/carritoCompras', Carritos::class)->name('carritoCompras');
Route::get('/carrito/{id}', Carritos::class)->name('livewire.carrito');
Route::get('/remove/{rowId}', Carritos::class)->name('remove');




// Route::get('/carrito' , [CarritoController::class,'index'])->name('carrito.index');
// ->middleware(['auth:sanctum','verified'])
// Route::get('/checkout', Checkout::class);

Route::get('/checkout', [CheckoutController::class,'index']);

Route::get('/contacto', Contactos::class);

// Route::get('/shop', [ShopController::class, 'index']);

// Route::get('/detalles', Detalles::class);

Route::get('/detalles/{producto}', [ProductoController::class, 'show'])->name('detalles.show');
/**RUTAS DE API DE FACEBOOK  */

Route::get('auth/{provider}', [SocialAuthController::class,'redirectToProvider' ])->name('social.auth');
Route::get('auth/{provider}/callback', [SocialAuthController::class,'handleProviderCallback' ]);
